import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstComponent} from './first/first.component';
import { SecondComponent} from './second/second.component';

const routes: Routes = [
  {path: 'FirstComponent', component: FirstComponent},
  {path: 'SecondComponent', component: SecondComponent},
  {path: '**', component: FirstComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
